/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './App';
import {name as appName} from './app.json';
import { Provider } from 'react-redux';
import store from 'redux/store';

function HeadlessCheck({ isHeadless }) {
    if (isHeadless) return null;

    return (
        <Provider store={store}>
            <App />
        </Provider>
    )
}

AppRegistry.registerComponent(appName, () => HeadlessCheck);

