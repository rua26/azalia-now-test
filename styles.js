import Constant from "theme/Constant";
import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
    txt: {
        color: Constant.PRIMARY_COLOR,
        fontFamily: "Roboto",
        fontStyle: "normal",
        fontSize: 16,
    }
});

export default styles;