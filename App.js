import React, { useEffect, useState } from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import HomeScreen from 'screens/HomeScreen/HomeScreen';
import LoadingScreen from 'screens/LoadingScreen/LoadingScreen';
import NewItemScreen from 'screens/NewItemScreen/NewItemScreen';
import { delay } from 'utils/helps';

const Stack = createStackNavigator();

const App = () => {
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    //Загрузка приложение за 2 секунд
    const initData = async() => {
      await delay(2000);
      setLoading(false);
    };

    initData();
  }, []);

  return (
    <NavigationContainer>
      <Stack.Navigator>
        {loading ?
          <Stack.Screen
            name={"LoadingScreen"}
            component={LoadingScreen}
            options={{ headerShown: false }}
          />
          :
          <Stack.Group>
            <Stack.Screen
              name={"Home"}
              component={HomeScreen}
              options={{ headerShown: false }}
            />
            <Stack.Screen
              name={"New to do"}
              component={NewItemScreen}
              options={{ title: "Вернуться назад" }}
            />
          </Stack.Group>
        }
      </Stack.Navigator>
    </NavigationContainer>
  )
};
export default App;
