import React from "react";
import { StatusBar, View } from "react-native";
import { SafeAreaView } from "react-native-safe-area-context";
import styles from "./styles";

const MainLayout = ({ children }) => {
    return (
        <SafeAreaView style={styles.container}>
            <View style={styles.page}>
                {children}
            </View>
        </SafeAreaView>
    );
};

export default MainLayout;