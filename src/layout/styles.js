import Constant from "theme/Constant";
import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Constant.BG_COLOR,
        paddingHorizontal: 20,
        paddingBottom: 34,
    },

    page: {
        flex: 1, 
        justifyContent: 'center',
        alignItems: 'center',
    }
});

export default styles;