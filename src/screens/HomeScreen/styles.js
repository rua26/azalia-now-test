import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
    container: {
        flex: 1,
        width: '100%',
        marginTop: 90,
        justifyContent: 'space-between'
    },

    logo: {
        width: 234,
        height: 36,
    },

    list: {
        marginTop: 19,
    },

    addNewTask: {
        alignItems: 'flex-end',
        marginRight: 16,
        
    }
});

export default styles;