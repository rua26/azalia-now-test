import React from "react";
import { View, Image, Text, FlatList } from 'react-native';
import TodoItem from 'components/TodoItem/TodoItem';
import MainLayout from 'layout/MainLayout';
import styles from './styles';
import { TouchableOpacity } from "react-native-gesture-handler";
import { useSelector } from "react-redux";
import { primaryTxt } from "theme/base";

const HomeScreen = ({ navigation }) => {
    const task = useSelector((state) => state.task);

    return (
        <MainLayout>
            <View style={styles.container}>
                <View style={styles.content}>
                    <Image source={require("assets/logo.png")} style={styles.logo} />
                    <View style={styles.list}>
                        {task.length > 0 ? (
                            <FlatList
                                data={task}
                                renderItem={({ item }) => <TodoItem item={item} />}
                                keyExtractor={item => item.id}
                            />
                        ) : (
                            <Text style={primaryTxt}>
                                У тебя нет задачи!!!
                            </Text>
                        )}
                    </View>
                </View>
                <View style={styles.addNewTask}>
                    <TouchableOpacity onPress={() => navigation.navigate("New to do")}>
                        <Image source={require("assets/add_new_task.png")} />
                    </TouchableOpacity>
                </View>
            </View>
        </MainLayout>
    );
};

export default HomeScreen;