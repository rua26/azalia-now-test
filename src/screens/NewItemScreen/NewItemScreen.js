import NewTask from 'components/NewTask/NewTask';
import MainLayout from 'layout/MainLayout';
import React from 'react';

const NewItemScreen = () => {
    return (
        <MainLayout>
            <NewTask />
        </MainLayout>
    );
};

export default NewItemScreen;