import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
    activityIndicator: {
        marginTop: 48,
    }
});

export default styles;