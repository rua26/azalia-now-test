import MainLayout from 'layout/MainLayout';
import React from 'react';
import {
    Image,
    ActivityIndicator,
    View
} from 'react-native';
import Constant from 'theme/Constant';
import styles from './styles';

const LoadingScreen = () => {
    return (
        <MainLayout>
            <Image source={require('assets/logo.png')} />
            <View style={styles.activityIndicator}>
                <ActivityIndicator size="large" color={Constant.SECONDARY_COLOR} />
            </View>
        </MainLayout>
    );
};

export default LoadingScreen;