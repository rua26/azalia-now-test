import { createSlice } from "@reduxjs/toolkit";

const initialState = [];

const taskSlice = createSlice({
    name: "task",
    initialState,
    reducers: {
        addNewTask: (state, action) => {
            return [
                ...state,
                action.payload,
            ];
        },
        deleteTask: (state, action) => {
            return state.filter((item) => item.id !== action.payload);
        },
        completedTask: (state, action) => {
            return state.map((item) => {
                if(item.id === action.payload) {
                    item = {
                        ...item,
                        completed: !item.completed,
                    };
                };
                return item;
            })
        },
    }
});

export const {
    addNewTask,
    deleteTask,
    completedTask,
} = taskSlice.actions;

export default taskSlice.reducer;