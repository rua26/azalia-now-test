import React from 'react';
import {
    View,
    Text,
    Animated,
} from 'react-native';
import Swipeable from "react-native-gesture-handler/Swipeable";
import styles from './styles';
import CheckBox from '@react-native-community/checkbox';
import { primaryTxt } from 'theme/base';
import Constant from "theme/Constant";
import { useDispatch } from 'react-redux';
import { completedTask, deleteTask } from 'redux/slices/taskSlice';
import { TouchableOpacity } from 'react-native-gesture-handler';

const TodoItem = ({ item }) => {
    const dispatch = useDispatch();

    // Позволяет удалять конкретный элемент по свайпу влево или вправо
    const deleteSwipe = (progress, dragY) => {
        const trans = dragY.interpolate({
            inputRange: [0, 100],
            outputRange: [0, 1],
            extrapolate: 'clamp',
        });

        return (
            <Animated.View
                style={[
                    styles.deleteAction,
                    {
                        transform: [{ translateY: trans }],
                    },
                ]}
            >   
                <Text style={[primaryTxt, { color: 'white' }]}>Delete</Text>
            </Animated.View>
        )
    };

    return (
        <Swipeable 
            renderRightActions={deleteSwipe}
            renderLeftActions={deleteSwipe}
            onSwipeableOpen={() => dispatch(deleteTask(item.id))}
        >
            <View style={styles.container}>
                <CheckBox
                    disabled={false}
                    value={item.completed}
                    tintColors={{ true: Constant.CHECKBOX_COLOR, false: Constant.CHECKBOX_COLOR }}
                    boxType="square"
                    onFillColor={Constant.CHECKBOX_COLOR}
                    onTintColor={Constant.CHECKBOX_COLOR}
                    onCheckColor="white"
                    onValueChange={() => dispatch(completedTask(item.id))}
                />
                <Text style={[primaryTxt, styles.todoTxt, item.completed && styles.completedTask]}>{item.title}</Text>
            </View>
        </Swipeable>
    );
};

export default TodoItem;