import { StyleSheet } from "react-native";
import Constant from "theme/Constant";

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: Constant.BG_TODO_ITEM,
        borderRadius: 8,
        paddingHorizontal: 9,
        paddingVertical: 12,
        marginBottom: 16,
        shadowColor: 'black',
        shadowRadius: 8,
        shadowOffset: {
            width: 4,
            height: 0,
        }
    },

    todoTxt: {
        marginHorizontal: 16,
        paddingRight: 16,
    },

    completedTask: {
        opacity: 0.5,
        textDecorationStyle: 'solid',
        textDecorationLine: 'line-through',
    },

    deleteAction: {
        flex: 1,
        borderRadius: 8,
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 16,
        backgroundColor: Constant.DANGEROUS_STATUS,
        paddingHorizontal: 16,
    }

});

export default styles;