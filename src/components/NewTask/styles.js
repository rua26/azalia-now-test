import { StyleSheet } from "react-native";
import Constant from "theme/Constant";

const styles = StyleSheet.create({
    container: {
        width: '100%'
    },

    inputField: {
        width: '100%',
        paddingVertical: 8,
        paddingHorizontal: 13,
        backgroundColor: Constant.BG_TODO_ITEM,
        borderRadius: 8,
        shadowColor: 'red',
        shadowRadius: 8,
        shadowOffset: {
            width: 4,
            height: 0,
        }
    },

    newTaskBtn: {
        marginTop: 16,
        backgroundColor: Constant.CHECKBOX_COLOR,
        paddingVertical: 8,
        borderRadius: 8,
        textAlign: 'center'
    },

    btnTxt: {
        color: '#ffffff',
        textAlign: 'center'
    },

    emptyInput: {
        opacity: 0.5,
    },

    statusTxt: {
        fontSize: 20,
        marginBottom: 10,
        color: Constant.SUCCESSFUL_STATUS
    }
});

export default styles;