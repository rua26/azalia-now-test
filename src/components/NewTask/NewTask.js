import React, { useState } from "react";
import {
    TextInput,
    View,
    Text,
    TouchableOpacity,
} from 'react-native';
import { useDispatch } from "react-redux";
import { primaryTxt } from "theme/base";
import { addNewTask } from "redux/slices/taskSlice";
import styles from "./styles";

const NewTask = () => {
    const dispatch = useDispatch();
    const [todo, setTodo] = useState("");
    const [status, setStatus] = useState(null);

    // добавить задачу через инпут поля
    const handleAddNewTask = () => {
        if (todo !== "") {
            const id = Math.floor(1 + Math.random() * 900000);
            dispatch(addNewTask({
                id,
                title: todo,
                completed: false,
            }));
            setTodo("");
            setStatus("Вы добавили новую зачачу.")
        };
    };

    const handleInputNewTask = (text) => {
        // после добавления нового элемента в список, поле должно очищаться
        setStatus(null);
        setTodo(text);
    }

    return(
        <View style={styles.container}>
            {status && (
                <Text style={[primaryTxt, styles.statusTxt]}>{status}</Text>
            )}
            <View>
                <TextInput
                    value={todo}
                    onChangeText={handleInputNewTask} 
                    placeholder="Добавь новую задачу"
                    style={[primaryTxt, styles.inputField]}
                />
            </View>
            <TouchableOpacity
                style={[styles.newTaskBtn, todo === "" && styles.emptyInput]}
                onPress={handleAddNewTask}
            >
                <Text style={[primaryTxt, styles.btnTxt]}>Добавить</Text>
            </TouchableOpacity>
        </View>
    );
};

export default NewTask;