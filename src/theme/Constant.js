export default class Constant {
    static PRIMARY_COLOR = '#222F3E';
    static SECONDARY_COLOR = '#FF003C';
    static TEXT_PRIMARY = '#222F3E';
    static BG_COLOR = '#E5E5E5';
    static SHADOW_COLOR = '#000000';
    static BG_TODO_ITEM = '#FAFAFE';
    static CHECKBOX_COLOR = '#222F3E';
    static SUCCESSFUL_STATUS =  '#008000';
    static DANGEROUS_STATUS = '#e62e00';
}