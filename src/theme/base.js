import Constant from 'theme/Constant';
  
export const primaryTxt  = {
    color: Constant.PRIMARY_COLOR,
    fontFamily: "Roboto",
    fontStyle: "normal",
    fontSize: 24,
    lineHeight: 28,
    fontWeigth: 400,
};
